#!/usr/bin/perl -w
use strict;
use feature 'say';
use Date::Calc qw(Day_of_Week);

use Mojo::UserAgent;
use Mojo::DOM;
my $ua = Mojo::UserAgent->new;

#say $ua->get('http://www.moya-planeta.ru/xml/small.xml' => {DNT => 1})->res->body;

my $dom = Mojo::DOM->new( $ua->get('http://www.moya-planeta.ru/xml/small.xml' => {DNT => 1})->res->body );

my @wday = ('Понедельник','Вторник','Среда','Четверг','Пятница','Суббота','Воскресенье');
my @month = ('января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');

my $program;
my $title;

# Loop
for my $e ($dom->find('Event')->each) {
	# Определяем дату и время
	$e->Start->text =~ /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):\d{2}/;
	my $date = "$1-$2-$3";
	my $date_title = $wday [ Day_of_Week($1,$2,$3) - 1 ] . ", $3 " . $month [ $2 - 1 ];
	my $time = "$4:$5";
#	say $date,$time,$date_title;
#	say $e->Title->text;

	my $image = ( $e->find('Image')->size ? $e->Image->text : undef );
	$title->{$date} = $date_title;
	push @{$program->{$date}},
		{
			time => $time,
			title => $e->Title->text,
			info => $e->Info->text,
			image => $image,
		}
}

use Data::Dumper;

#print Dumper($program);

use utf8;
my $template = <<'EOF';
% my ($program,$title) = @_;
<html>
<title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
</title>
<body>

% foreach my $date (sort keys %$program) {
<h2><%= $title->{$date} %></h2>

% foreach my $event (@{$program->{$date}}) {
<p style="margin: 0;">
<b><%= $event->{time} %></b> <b><%= $event->{title} %></b> <%= $event->{info} %>

% if ($event->{image}) {
<br><img src="<%= $event->{image} %>">
% }

</p>
% }

% }

</body>
</html>


EOF

use Mojo::Template;
my $mt = Mojo::Template->new;
#my $encoding = $mt->encoding;
#  $mt          = $mt->encoding('UTF-8');
  
# Simple
my $output = $mt->render($template,$program,$title);

use utf8;
print $output;